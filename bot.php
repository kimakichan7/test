<?php
date_default_timezone_set("UTC");

$useragent="Mozilla/5.0 (Linux; Android 11; M2003J15SC) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Mobile Safari/537.36";
$cookie="_ga=GA1.1.29058996.1684158051; _ga_S8JDHSWJYS=GS1.1.1684341084.4.1.1684341316.0.0.0";
$address="0xcf4789c69c9351a186C0F6f598fE7c14D4EBbB17";
$ref="https://swaprum.finance/?ref=1b6dbb4d";

//error_reporting(0);
// color
$green = "\e[1;32m";
$blockglow = "\033[102m";
$blue = "\e[1;34m";
$blockblue = "\033[104m";
$red = "\e[1;31m";
$blockred = "\033[101m";
$blockpink = "\033[105m";
$white = "\33[37;1m";
$blockwhite = "\033[107m";
$yellow = "\e[1;33m";
$blockyellow = "\033[103m";
$cyan = "\e[1;36m";
$blockcyan = "\033[106m";
$purple = "\e[1;35m";
$gray = "\e[1;30m";
$blockgray = "\033[100m";
$end = "\033[0m";
$orange = "\033[38;5;202m";

function banner0(){
$gray = "\e[1;30m";
echo "\n";
echo$gray."  ██"; usleep(10000); echo$gray."╗   "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╗"; usleep(10000); echo$gray."███████"; usleep(10000); echo$gray."╗"; usleep(10000); echo$gray."███████"; usleep(10000); echo$gray."╗"; usleep(10000); echo$gray."███████"; usleep(10000); echo$gray."╗"; usleep(10000); echo$gray."████████"; usleep(10000); echo$gray."╗"; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╗   "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╗\n";    
echo$gray."  ╚"; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╗ "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╔╝╚══"; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝╚══"; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝╚══"; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝╚══"; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╔══╝"; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."║   "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."║\n";    
echo$gray."   ╚"; usleep(10000); echo$gray."████"; usleep(10000); echo$gray."╔╝   "; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝   "; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝   "; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝    "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."║   "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."║   "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."║\n";    
echo$gray."    ╚"; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╔╝   "; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝   "; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝   "; usleep(10000); echo$gray."███"; usleep(10000); echo$gray."╔╝     "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."║   ╚"; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╗ "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."╔╝\n";    
echo$gray."     ██"; usleep(10000); echo$gray."║   "; usleep(10000); echo$gray."███████"; usleep(10000); echo$gray."╗"; usleep(10000); echo$gray."███████"; usleep(10000); echo$gray."╗"; usleep(10000); echo$gray."███████"; usleep(10000); echo$gray."╗   "; usleep(10000); echo$gray."██"; usleep(10000); echo$gray."║    ╚"; usleep(10000); echo$gray."████"; usleep(10000); echo$gray."╔╝\n"; 
echo$gray."     ╚═╝   ╚══════╝╚══════╝╚══════╝   ╚═╝     ╚═══╝\n";
//sleep(1);
system('clear');
}

function banner(){
$gray="\e[1;30m";
$green="\e[1;32m";
$red="\e[1;31m";
$white="\33[37;1m";
$yellow="\e[1;33m";
$cyan="\e[1;36m";
$blockmerah="\033[101m";
$end="\033[0m";
$str=str_repeat("🔥",27);
echo$str." \n";
echo$gray."░░".$red."██".$yellow."╗".$gray."░░░".$red."██".$yellow."╗".$red."███████".$yellow."╗".$red."███████".$yellow."╗".$red."███████".$yellow."╗".$red."████████".$yellow."╗".$red."██".$yellow."╗".$gray."░░░".$red."██".$yellow."╗\n";
echo$gray."░░".$yellow."╚".$red."██".$yellow."╗".$gray."░".$red."██".$yellow."╔╝╚══".$red."███".$yellow."╔╝╚══".$red."███".$yellow."╔╝╚══".$red."███".$yellow."╔╝╚══".$red."██".$yellow."╔══╝".$red."██".$yellow."║".$gray."░░░".$red."██".$yellow."║\n";
echo$gray."░░░".$yellow."╚".$red."████".$yellow."╔╝".$gray."░░░".$red."███".$yellow."╔╝".$gray."░░░".$red."███".$yellow."╔╝". $gray."░░░".$red."███".$yellow."╔╝".$gray."░░░░".$red."██".$yellow."║".$gray."░░░".$red."██".$yellow."║".$gray."░░░".$red."██".$yellow."║\n";
echo$gray."░░░░".$yellow."╚".$white."██".$yellow."╔╝".$gray."░░░".$white."███".$yellow."╔╝".$gray."░░░".$white."███".$yellow."╔╝". $gray."░░░".$white."███".$yellow."╔╝".$gray."░░░░░".$white."██".$yellow."║".$gray."░░░".$yellow."╚".$white."██".$yellow."╗".$gray."░".$white."██".$yellow."╔╝\n";
echo$gray."░░░░░".$white."██".$yellow."║".$gray."░░░".$white."███████".$yellow."╗".$white."███████".$yellow."╗".$white."███████".$yellow."╗".$gray."░░░".$white."██".$yellow."║".$gray."░░░░╚".$white."████".$yellow."╔╝".$gray."░\n";
echo$gray."░░░░░".$yellow."╚═╝".$gray."░░░".$yellow."╚══════╝╚══════╝╚══════╝".$gray."░░░".$yellow."╚═╝". $gray."░░░░░".$yellow."╚═══╝".$gray."░░\n";
echo$str." \n";
}
function strip2(){
$red = "\e[1;31m";
$strip2 = str_repeat($red."◼",54);
echo $strip2."\n";
}

function strip3(){
$red = "\e[1;31m";
$strip3 = str_repeat($red."◼",17);
echo $strip3;
}

function strip4(){
$red = "\e[1;31m";
$strip4 = str_repeat($red."◼",18);
echo $strip4." \n";
}

function sruput(){
$green = "\e[1;32m";
$blockglow = "\033[102m";
$blue = "\e[1;34m";
$blockblue = "\033[104m";
$red = "\e[1;31m";
$blockred = "\033[101m";
$blockpink = "\033[105m";
$white = "\33[37;1m";
$blockwhite = "\033[107m";
$yellow = "\e[1;33m";
$blockyellow = "\033[103m";
$cyan = "\e[1;36m";
$blockcyan = "\033[106m";
$purple = "\e[1;35m";
$gray = "\e[1;30m";
$blockgray = "\033[100m";
$end = "\033[0m";
$orange = "\033[38;5;202m";

echo$yellow."Sruput name    : ".$blockred.$white."SWAPRUM FREE ARB\n".$end;
echo$yellow."Created by     : ".$red."▓".$white."▓ ".$cyan."© ".$green."YzZz Tv\n";
echo$yellow."YzZzTv YT      : ".$green."https://youtube.com/c/YzZzTv\n";
echo$yellow."Supported by   : ".$blockyellow."🐱".$end.$orange." KOCHENG OREN SCRIPTER TEAM\n";
echo$yellow."Donate TRX     : ".$green."TCGDC9U4vfqEGaoPUB6QV9wYLLseXJUexE\n";
}

function Get($url, $ua){
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_ENCODING, "");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_HTTPHEADER, $ua);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$result = curl_exec($ch);
curl_close($ch);
return $result; 
sleep(1);
}

function Post($link, $ua, $data){
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $link);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, $ua);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_COOKIEJAR, ".session/cookie.txt");
curl_setopt($ch, CURLOPT_COOKIEFILE, ".session/cookie.txt");
$result = curl_exec($ch);
curl_close($ch);
return $result; 
}







system("clear");
banner0();
banner();
strip2();
sruput();
strip2();

$url="https://swaprum.finance/server/user?address=".$address."&ref=".$ref;
$ua=["user-agent: ".$useragent,"cookie: ".$cookie,"referer: https://swaprum.finance/free-tokens","authority: swaprum.finance","scheme: https"];
$d=Get($url,$ua);

$wallet=explode('"',explode('"address":"',$d)[1])[0];
$tele=explode('"',explode('"telegramNameFL":"',$d)[1])[0];
$twit=explode('"',explode('"twitterUserName":"',$d)[1])[0];
$bal0=explode('"',explode('"freeClaimBalance":"',$d)[1])[0];
$balx=$bal0/1000000000000000000;
$bal=number_format($balx,8);

echo$green."Connected account:\n";
echo$white."Address  : ".$yellow.$wallet." \n";
echo$white."Telegram : ".$yellow.$tele." \n";
echo$white."Twitter  : ".$yellow.$twit." \n";
echo$white."Balance  : ".$yellow.$bal." ARB\n";

strip2();

while(true){

//load-dashboard
$url="https://swaprum.finance/server/user?address=".$address."&ref=".$ref;
$ua=["user-agent: ".$useragent,"cookie: ".$cookie,"referer: https://swaprum.finance/free-tokens","authority: swaprum.finance","scheme: https"];
$d=Get($url,$ua);

//check-last-claim
$url="https://swaprum.finance/server/free-token?address=".$address;
$ua=["user-agent: ".$useragent,"cookie: ".$cookie,"referer: https://swaprum.finance/free-tokens","authority: swaprum.finance","scheme: https"];
$t=Get($url,$ua); 
$timer=explode(',',explode('lastClaimTime":',$t)[1])[0];
$amt0=explode('"',explode('rateHour":"',$t)[1])[0];
$amtx=$amt0/1000000000000000000;
$amt=number_format($amtx,8);
$datee=date("i",$timer);
$now=date("i");

//execute-claim
$url="https://swaprum.finance/server/claim-free?address=".$address;
$ua=["user-agent: ".$useragent,"cookie: ".$cookie,"referer: https://swaprum.finance/free-tokens","authority: swaprum.finance","scheme: https"];
$c=Get($url,$ua);

echo$green."Claimed ".$yellow.$amt." ARB ! -";

$url="https://swaprum.finance/server/user?address=".$address."&ref=".$ref;
$ua=["user-agent: ".$useragent,"cookie: ".$cookie,"referer: https://swaprum.finance/free-tokens","authority: swaprum.finance","scheme: https"];
$d2=Get($url,$ua);
$bal0=explode('"',explode('"freeClaimBalance":"',$d2)[1])[0];
$balx=$bal0/1000000000000000000;
$bal=number_format($balx,8);

echo$white."Bal : ". $yellow.$bal." ARB \n";

//check-last-claim
$url="https://swaprum.finance/server/free-token?address=".$address;
$ua=["user-agent: ".$useragent,"cookie: ".$cookie,"referer: https://swaprum.finance/free-tokens","authority: swaprum.finance","scheme: https"];
$t=Get($url,$ua); 
$timer=explode(',',explode('lastClaimTime":',$t)[1])[0];
$amt0=explode('"',explode('rateHour":"',$t)[1])[0];
$amtx=$amt0/1000000000000000000;
$amt=number_format($amtx,8);
$datee=date("i",$timer);
$now=date("i");


for($x=3600;$x>0;$x--){echo "\r \r";
echo$gray." Next claim in ".$red."[".$yellow.$x.$red."] ". $gray."seconds...   ";
echo "\r \r";
sleep(1);}




}
